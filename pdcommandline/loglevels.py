# definition of log levels
LOG_FATAL   = 0
LOG_ERROR   = 1
LOG_WARN    = 2
LOG_INFO    = 3
LOG_INSIGHT = 4
LOG_DEBUG   = 5

# definition of maximum log level
LOG_MAX_LEVEL = LOG_DEBUG

# logical log prio mappings
LOG_ALWAYS  = 0
LOG_DEFAULT = LOG_INFO