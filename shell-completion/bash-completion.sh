# completion helper for bash shells

# evaluate possible completions for the current command line
_pdcommandline_completions()
{
    # check cursor position
    WORD_COUNT=${#COMP_WORDS[@]}
    CHAR_BEFORE_CURSOR=${COMP_LINE:$(($COMP_POINT-1)):1}
    CHAR_AT_CURSOR=${COMP_LINE:$COMP_POINT:1}

    # invoke command and get possible completion options
    CMD=${COMP_WORDS[0]}
    SPACE_BEFORE=0
    if [[ "x$CHAR_BEFORE_CURSOR" == "x " ]] || [[ "x$CHAR_BEFORE_CURSOR" == "x\t" ]]; then
        SPACE_BEFORE=1
    fi
    if [ "$SPACE_BEFORE" == "1" ]; then
        COMP_CWORD=$(($COMP_CWORD-1))
        VALUES=$($CMD "@extend@command@line@context@" ${COMP_WORDS[@]:1:$COMP_CWORD} "" )
        RESULT=$?
    else
        VALUES=$($CMD "@extend@command@line@context@" ${COMP_WORDS[@]:1:$COMP_CWORD} )
        RESULT=$?
    fi

    # publish completions
    if [ "x$RESULT" == "x0" ] && [ "x$VALUES" != "x" ]; then
        readarray -t COMPREPLY < <(echo "$VALUES")
    fi

    # debug completion handler
    DEBUG=0
    if [ "x$DEBUG" != "x0" ]; then
        echo "CMD: $CMD" > /tmp/completion.log
        echo "LINE: \"$COMP_LINE\"" >> /tmp/completion.log
        echo "INVOKE: \"$CMD @extend@command@line@context@ ${COMP_WORDS[@]:1:$COMP_CWORD}\"" >> /tmp/completion.log
        echo "COMP_KEY: \"$COMP_KEY\"" >> /tmp/completion.log
        echo "COMP_POINT: $COMP_POINT" >> /tmp/completion.log
        echo "CHAR_BEFORE_CURSOR: \"$CHAR_BEFORE_CURSOR\"" >> /tmp/completion.log
        echo "COMP_CURRENT: \"${COMP_WORDS[$COMP_CWORD]}\"" >> /tmp/completion.log
        echo "COMP_CWORD: $COMP_CWORD" >> /tmp/completion.log
        echo "COMP_WORD_COUNT: $WORD_COUNT" >> /tmp/completion.log
        echo "RESULT: $RESULT" >> /tmp/completion.log
        echo "VALUES:" >> /tmp/completion.log
        echo "${VALUES}" >> /tmp/completion.log
    fi
}

# register completion function
# TODO: replace test.py with script file name
complete -F _pdcommandline_completions testformatter.py
